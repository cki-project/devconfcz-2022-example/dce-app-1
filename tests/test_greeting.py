"""Tests for the greeting."""

import unittest
from unittest import mock

from dce_app_1 import greeting


class TestHtml(unittest.TestCase):
    """Tests for the greeting."""

    def test_name(self) -> None:
        """Check the user name."""
        with greeting.app.test_client() as client:
            self.assertIn(b'baz', client.get('/baz').data)
            self.assertIn(b'stranger', client.get('/').data)

    @mock.patch.dict('os.environ', {'DCE_ENVIRONMENT': 'production'})
    def test_production(self) -> None:
        """Check the production marker."""
        with greeting.app.test_client() as client:
            response = client.get('/baz')
        self.assertIn(b'production', response.data)

    def test_non_production(self) -> None:
        """Check the production marker."""
        with greeting.app.test_client() as client:
            response = client.get('/baz')
        self.assertIn(b'staging', response.data)
