"""First example (greeting) app."""
import pathlib

from dce_common import html
from dce_common import misc
import flask

app = flask.Flask(__name__)


def _hello(name: str) -> str:
    doctype = '<!doctype html>'
    title = f'<title>{__name__} - production == {misc.is_production()}</title>'
    css = '<style>p { color: green; }</style>'
    environment = f'<h1>{misc.deployment_environment()}</h1>'
    greeting = html.hello_world(name)
    code = f'<small>{pathlib.Path("/etc/dce-image").read_text(encoding="utf8")}</small>'

    return doctype + title + css + environment + greeting + code


@app.route('/', methods=['GET'])
def hello_stranger() -> str:
    """Process a get request with an anonymous greeting."""
    return _hello('stranger')


@app.route('/<name>', methods=['GET'])
def hello(name: str) -> str:
    """Process a get request with a personalized greeting."""
    return _hello(name)
